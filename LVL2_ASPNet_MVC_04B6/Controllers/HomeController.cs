﻿using LVL2_ASPNet_MVC_04B6.Models;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_04B6.Controllers
{
    public class HomeController : Controller
    {

        db_customer_dicky_user db = new db_customer_dicky_user();

        public ActionResult About()
        {
            return View();
        }

        public ActionResult GetData(tbl_user_dicky user)
        {
            user = db.tbl_user_dicky.Where(x => x.Id_User == 2).SingleOrDefault();
            return Json(user, JsonRequestBehavior.AllowGet);



            //var chk = new check
            //{
            //    subject = "hello! " + param1,
            //    description = param2 + " Years Old"
            //};
            //return JsonConvert.SerializeObject(chk);

            //ViewBag.Message = "Your application description page.";
            //return View();
        }

        public class check
        {
            public string subject { get; set; }
            public string description { get; set; }
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}